/* ----------------------------------------
	Port of most the Bookbot code
 ---------------------------------------- */

let dio		= require("../core/dio"),
	x		= require("../core/vars"),
	command = require("../core/command").Command,
	helpers = require("../core/helpers"),
	stripIndents = require("common-tags").stripIndents;

//Yay arrays
let ww_issues = [
	{
		name: "Issue 15",
		value: "[Read the latest issue of the Warren Weekly](http://bit.ly/2l3BgZj)",
		inline: false
	},
	{
		name: "Issue 12",
		value: "[Read an archived issue](http://bit.ly/2hjxude)",
		inline: true
	},
	{
		name: "Issue 11",
		value: "[Read an archived issue](http://bit.ly/2fbgODo)",
		inline: true
	},
	{
		name: "Issue 9",
		value: "[Read an archived issue](http://bit.ly/2dgEmU6)",
		inline: true
	},
	{
		name: "Issue 7",
		value: "[Read an archived issue](http://bit.ly/2aT5dGI)",
		inline: true
	},
	{
		name: "Issue 6",
		value: "[Read an archived issue](http://bit.ly/29wcywy)",
		inline: true
	},
	{
		name: "Issue 5",
		value: "[Read an archived issue](http://bit.ly/29Yz5TG)",
		inline: true
	},
	{
		name: "Issue 4",
		value: "[Read an archived issue](http://bit.ly/2aaf7BK)",
		inline: true
	},
	{
		name: "Issue 3",
		value: "[Read an archived issue](http://bit.ly/29JQD3Z)",
		inline: true
	},
	{
		name: "Issue 2",
		value: "[Read an archived issue](http://bit.ly/29SLMfC)",
		inline: true
	},
	{
		name: "Issue 1",
		value: "[Read an archived issue](http://bit.ly/29EtwWM)",
		inline: true
	}
];

let guides = [
	{
		name: "Introduction: A basic guide",
		value: "[Link to Lacante's Video](https://www.youtube.com/watch?v=w8Y2gdrgpUA)",
		inline: false
	},
	{
		name: "The Commander and You",
		value: "[Link (written by Glyde)](https://toothandtailwiki.com/guides/glydes-beginner-guide/)",
		inline: true
	},
	{
		name: "Knowing the Battlefield",
		value: "[Link (written by Glyde)](https://toothandtailwiki.com/guides/glydes-beginner-guide/2)",
		inline: true
	},
	{
		name: "Economics of War",
		value: "[Link (written by Glyde)](https://toothandtailwiki.com/guides/glydes-beginner-guide/3)",
		inline: true
	},
	{
		name: "Meet your Comrades",
		value: "[Link (written by Glyde)](https://toothandtailwiki.com/guides/glydes-beginner-guide/4)",
		inline: true
	},
	{
		name: "The Subtleties of Battle",
		value: "[Link (written by Glyde)](https://toothandtailwiki.com/guides/glydes-beginner-guide/5)",
		inline: true
	},
	{
		name: "Unit Overview",
		value: "[Link to Shooflypi's video](https://www.youtube.com/watch?v=PmEPEuHRoJM)",
		inline: true
	}
];

let cmd_patch = new command("bookbot", "!patch", "See the most recent changes to the game", function(data){
	dio.say(""+
		// I love this.
		"Most recent Documented Changes:\n"+
		"<http://blog.pocketwatchgames.com/post/164612463831/tooth-and-tail-patch-notes-pre-alpha-29>\n", data);
});

let cmd_newspaper = new command("bookbot", "!newspaper", "Read the most recent issue of the Weekly Warren", function(data){
	let ww_embed = new helpers.Embed({
		title: "The Warren Weekly",
		color: "8281503",
		description: "The Warren Weekly is a newspaper written by Glyde in the Tooth and Tail universe explaining certain changes noted in the most recent patch notes.",
		footer: {
			text: "Last issue published on 17th February"
		}
	});

	for(var k in ww_issues){
		ww_embed.pushItem(new helpers.EmbedItem(ww_issues[k].name, ww_issues[k].value, ww_issues[k].inline));
	}

	dio.sendEmbed(ww_embed, data);
});

let cmd_troubleshoot = new command("bookbot", "!troubleshoot", "Troubleshoot common errors", function(data){
	if (data.args[1] == null) {
		dio.say(stripIndents`
			What seems to be the trouble with your game? Type in '!troubleshoot #' by choosing an option below:

			:one: Unable to load \`mf.dll\`
			:two: Game is slow on laptop
			:three: Steam crashes
			:four: White screen of death
			:five: Other`, data);
	} else {
		let k = data.args[1];
		let res;
		switch (k) {
		case ":one:":
		case "1":
		case "dll":
			res = "**Unable to load `mf.dll`**\nJust install this guy: <https://support.microsoft.com/en-us/help/4016817/media-feature-pack-for-n-edition-of-windows-10-version-1703-april-2017>";
			break;
		case ":two:":
		case "2":
		case "igp":
			res = "**Game is slow on laptop**\nIt's possible your laptop is running TnT off of the integrated card instead of the GPU. Try manually setting the game's .exe to specifically use the dedicated card.";
			break;
		case ":three:":
		case "3":
			res = "**Steam crashes**\nThis may be a problem with AVG or Avast. To play the game you can either temporarily disable the anti-virus program, or add `C:\\Program Files (x86)\\Steam\\steamapps\\common\\ToothAndTail\\` to the program's exemptions.";
			break;
		case ":four:":
		case "4":
		case "wsod":
			res = "**White screen of death**\nThere are a few things you can try: switch to integrated graphics, run in windowed mode, nuke your Options file. For the latter 2, you can find that file in `%AppData%\\ToothAndTail\\Options.xml` (Windows) or `~/.config/ToothAndTail/Options.xml` (Linux/Mac)";
			break;
		case ":five:":
		case "5":
			res = `**Other**\nFor any other problems, ask someone in <#${x.trouble}>. :sweat_smile:`;
			break;
		}

		dio.say(res, data);
	}
});

let cmd_guide = new command("bookbot", "!guide", "Get a useful list of guides for the game", function(data){
	let guide_embed = new helpers.Embed({
		title: "Guides",
		color: "8281503",
		description: "If you're new to the game, this is a great place to start and I hope by the end of this, you'll have a solid understanding of Tooth and Tail and be better equipped with knowledge to win your battles."
	});

	for(var k in guides){
		guide_embed.pushItem(new helpers.EmbedItem(guides[k].name, guides[k].value));
	}

	dio.sendEmbed(guide_embed, data);
});

let cmd_coc = new command("bookbot", "!coc", "Get Information on **Clash of Comrades**", function(data) {
	//Mew
	let coc_embed = new helpers.Embed({
		title: "<:tntwolf:253730191556214795> **Clash of Comrades** <:tntwolf:253730191556214795>",
		color: "8281503",
		description: "Clash of Comrades is a bi-monthly tournament for players of the game Tooth and Tail with the aim of friendly competition and the development of the game!",
		url: "http://clashofcomrades.com"
	});

	coc_embed.setImage("http://www.clashofcomrades.com/images/v71/banner2.png", 900, 200);

	coc_embed.pushItem(new helpers.EmbedItem("Rules & more", "[Read about the rules on facebook](https://www.facebook.com/ClashOfComrades)", true));
	coc_embed.pushItem(new helpers.EmbedItem("Latest Brackets", "[Find the latest brackets on challonge](http://clashofcomrades.challonge.com/)", true));
	coc_embed.pushItem(new helpers.EmbedItem("YouTube", "[Find Clash of Comrades on YouTube](https://www.youtube.com/channel/UCesgJAY8oYO9xxX_wR22WBg)", true));
	coc_embed.pushItem(new helpers.EmbedItem("Twitch", "[Find Clash of Comrades on Twitch](https://www.twitch.tv/clashofcomrades)", true));

	dio.sendEmbed(coc_embed, data);
});

let cmd_bookbot = new command("bookbot", "!bookbot", "Read up on Bookbot", function(data) {
	dio.say("Bookbot was a bot created by Glyde Borealis that used to be of great service for this community. His soul lives on in Pocketbot.", data);
});

module.exports.commands = [cmd_patch, cmd_newspaper, cmd_troubleshoot, cmd_guide, cmd_coc, cmd_bookbot];
