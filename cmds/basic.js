/* ----------------------------------------
	This file contains the basic example of
	a new command file. Also the !help
 ---------------------------------------- */

let logger  		= require("../core/logger"),
	command 		= require("../core/command").Command,
	commandSystem   = require("../core/command"),
	helpers 		= require("../core/helpers"),
	dio             = require("../core/dio");

let cmd_ping = new command("basic", "!ping", "Replies with pong and latency", function(data){
	data.bot.sendMessage({
		to: data.channelID,
		message: "🏓 Pong! One moment while I get my ping! :smiley:"
	}, function(error, response) {
		if(error) {
			logger.log(error, logger.MESSAGE_TYPE.Warn);
			return false;
		}
		data.bot.editMessage({
			channelID: data.channelID,
			messageID: response.id,
			message: "🏓 Pong! Latency: "+ Math.floor(new Date(response.timestamp).valueOf() - new Date(data.event.d.timestamp).valueOf()) +"ms!"
		});
	});
});

cmd_ping.permissions = [helpers.vars.admin];

var cmd_ding = new command("ryionbot", "!ding", "Replies with dong and latency", function(data){
	data.bot.sendMessage({
		to: data.channelID,
		message: "Dong! "+helpers.vars.emojis.nguyen+" One moment while I get my ping! :smiley:"
	}, function(error, response) {
		if(error) {
			logger.log(error, logger.MESSAGE_TYPE.Warn);
			return false;
		}
		data.bot.editMessage({
			channelID: data.channelID,
			messageID: response.id,
			message: "Dong! "+helpers.vars.emojis.nguyen+" Latency: "+ Math.floor(new Date(response.timestamp).valueOf() - new Date(data.event.d.timestamp).valueOf()) +"ms!"
		});
	});
});

cmd_ding.triggerType = commandSystem.TriggerType.InMessage;

let cmd_help = new command("ryionbot", "!help", "Sends the user a list of all command groups for further info", function(data){
	let k = (data.args[1]) ? data.args[1] : null;

	let helpText = "";
	if(k == null){
		helpText = data.commandManager.getHelp("bork", "all");
		dio.say(helpText, data, data.userID);
		return;
	}else if(data.commandManager.getCommand(k)){
		helpText = data.commandManager.getHelp(k, "command");
	}else{
		for(let gk in data.commandManager.groups){
			if(k == gk){
				helpText = data.commandManager.getHelp(k, "group");
			}
		}
	}

	if(!helpText) {
		dio.say("🕑 The help you requested does not exist!", data);
		return;
	}

	dio.say(helpText, data);
	return;
});

module.exports.commands = [cmd_ping, cmd_ding, cmd_help];
