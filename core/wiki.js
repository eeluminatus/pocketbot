let logger      = require("./logger"),
	fs          = require("fs"),
	WikiMedia   = require("mediawiki");

var bot = new WikiMedia.Bot({
	endpoint: "https://toothandtail.gamepedia.com/api.php",
	rate: 60e3 / 10,
	userAgent: "PocketBot <https://en.wiktionary.org/wiki/User:Mj0331>",
	byeline: "(Automatic action performed by PocketBot)"
});

//var unitData = "";
var lua_template = "";

module.exports.mwEditUnitData = function(){
	bot.login(process.env.WIKIMEDIA_USER, process.env.WIKIMEDIA_KEY).complete(function (username) {
		logger.log("Logged in to MediaWiki as " + username);

		fs.readFile("./assets/units.json", {encoding: "utf8"}, function(err, data){
			if (err){
				logger.log(err, "Error");
				return;
			}

			lua_template = `local TNT_DATA_JSON = [===[
${data}
]===]

return mw.text.jsonDecode(TNT_DATA_JSON,0)`;

			var editToken = null;
			var tokenRequest = bot.post({action: "query", meta:"tokens"});

			tokenRequest.complete(function(res){
				editToken = res.query.tokens.csrftoken;

				var timestamp = new Date(Date.now()).toISOString();
				var editRequest = bot.post({action: "edit", title: "Module:TNTGame/data", summary: "Bot autoupdate", basetimestamp: timestamp, text: lua_template, bot: true, token: editToken});

				editRequest.complete(function(){
					bot.page("Module:TNTGame/data").complete(function(title, text, date){
						logger.log("Checking contents of " + title, "Info");
						logger.log(text, "Info");
						logger.log(date, "Info");
					}).error(function(err){
						logger.log(err.toString(), "Error");
					});
				});

				editRequest.error(function(err){
					logger.log("Failed to update unit data.\n" + err.toString(), 3);
				});
			});

			tokenRequest.error(function(err){
				logger.log(err, 3);
			});
		});
	});
};
