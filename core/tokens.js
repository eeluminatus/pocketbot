require("dotenv").config({silent: true});

module.exports = {
	TOKEN: process.env.TOKEN,
	TWITCHID: process.env.TWITCHID,
	FBKEY: function() {
		if (process.env.hasOwnProperty("FBKEY")) {
			return process.env.FBKEY;
		} else { return false; }
	},
	SERVERID: function() {
		if(process.env.hasOwnProperty("SERVERID")) {
			return process.env.SERVERID;
		} else {return "99240110486192128";}
	},
	FBPKEY: function() {
		if (process.env.hasOwnProperty("FBPKEY")) {
			return process.env.FBPKEY.replace(/\\n/g, "\n");
		} else { return false; }
	},
	FBKEYEMAIL: function() {
		if (process.env.hasOwnProperty("FBKEYEMAIL")) {
			return process.env.FBKEYEMAIL;
		} else { return false; }
	},
	FBKEY2: function() {
		if (process.env.hasOwnProperty("FBKEY2")) {
			return process.env.FBKEY2;
		} else { return false; }
	},
	TWITKEY: process.env.TWITKEY,
	TWITTOKEN: process.env.TWITTOKEN,
	TWITATOKEN: process.env.TWITATOKEN,
	TWITSECRET: process.env.TWITSECRET
};
